# Infrastructure (Platypus)

The goal for this project is to deploy a few variations of our environment intended for different users in an easier programmatic way while reducing costs and with CI & logging integrated. The stack might seem heavy but this isn't intended for a single machine but to orchestrate 100s of physical machines and 1000s of application services.

## Tech Choices

* [CentOS](https://www.centos.org/) (have to use this, not my choice)
* [ELKB](http://elastic.co) (Elasticsearch, Logstash, Kibana, Beats)
  * [Packetbeat](https://www.elastic.co/downloads/beats/packetbeat)
  * [Filebeat](https://www.elastic.co/downloads/beats/filebeat)
  * [Topbeat](https://www.elastic.co/downloads/beats/topbeat)
* [Buildbot](http://buildbot.net/)
* [Gitlab](https://gitlab.com/)
* [Vagrant](https://www.vagrantup.com/)
* [Docker](httsp://docker.com) [1]
* [Consul](https://consul.io)
  * [Consulate](https://github.com/gmr/consulate)
* [Packer](https://packer.io)
* [Terraform](https://terraform.io)
* [Vault](https://vaultproject.io)
* [Serf](https://serfdom.io)
* [MariaDB](https://mariadb.org) & [Vitess](https://vitess.io)
* [collectD](https://collectd.org)
* [statsD](https://github.com/etsy/statsd)

### Ansible
Our reason for using this is fairly simple. RedHat has acquired ansible, which means at some point it will become the de-facto standard for the RHEL/CentOS ecosystem. Aside from this, ansible doesn't rely on having a master node like puppet does most of the time; Honestly we use ansible locally and not for its orchestration ability as we have something else handling that.

### AWS
Based on our use case, I was hesitant to offload anything into AWS, but I would like to have this project 'decide' if it is more cost efficient to provision something in AWS or in our cluster. So we are also gonna be using Cisco's [terraform.py](https://github.com/CiscoCloud/terraform.py) project to coordinate with the AWS pricing page to ultimately determine where to provision and re-evaluate existing services to be moved if the cost makes sense that day.

## Goals
I want to be able to allow my team to work together, provision servers, get live metrics, and overall make it easier to manage our cluster while saving on costs and meeting stakeholder requirements to utilize cloud services.

### Use-Case images
We do have a requirement to have different types of images for different users; Specifically we need to provide images for service providers, developers/users, admins, and kiosk-mode (for demos). When you view our playbooks, you will see conditional statements related to this.

### Pipeline
We start with application code triggered by `git push`, `vagrant push`, `CI push`, or ***shudder*** `FTP`; This leads to **Packer** building images for both physical machines and creating AMIs. Then this leads to the artifacts being registered followed by deployment by **Terraform**. This event triggers **Consul** to update our applications such as Elasticsearch, etc. Vault manages the secrets in this pipeline  

## Versioning
Versioning is solely based on [SemVer](http://semver.org/). Major versions have names of endangered animals.

## Notes
[1]: VMs and Containers are not mutually exclusive. If you disagree, I direct you to this statement by Solomon Hykes:

>"The first thing that happens when people play with containers in their development projects is it looks like a VM but faster and more lightweight and consuming less memory. And those are all true," Hykes said. "Several years before Docker existed, a common, wisdom among [IT pros and developers] was a container was just that: smaller, more lightweight, a faster VM. The fundamental difference between Docker and other lower-level container tools is simply we disagree. We think containers and VMs are fundamentally different. They operate at different levels of the stack and as a result they are not mutually exclusive. You can use containers with VMs, you can use containers without VMs, directly on bare metal, and we're seeing organizations do both."

We utilize containers as easy management of logical resources (i.e. application resources) while VMs help to orchestrate the physical resources (i.e. bare metal servers, ec2 instances, etc). If you disagree with this as well, I direct you yet again to Solomon:

> "Typically what we've seen is the way developers reason about containers is not as a possible replacement for VMs, but as an additional layer on top of their infrastructure, which allows them to pick and choose the best infrastructure for each job. [This] means it now becomes easier for an organization to use VMs where VMs make sense, to use bare metal when bare metal makes sense and, of course, to pick and choose between multiple physical machine providers and virtual machine providers and then layer on top of all these different parts of their infrastructure. On top of which they can express their applications. So the bottom line is containers are for applications and VMs are for machines."
