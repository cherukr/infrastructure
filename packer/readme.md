# Infrastructure default box

## Bare Metal

## AWS
The base AMI used for the default box is "CentOS 7 (x86_64) with Updates HVM"
which then undergoes a full `yum upgrade` before being provisioned by
master-less Puppet.

The image proposed by this repo is entirely usable and will likely not change
significantly going forward.  

## SSHd

SSHd is configured to run on both ports `22` and `22123`; the former is meant
for internal (private) use only, while the latter is meant for external
(public) exposure.
