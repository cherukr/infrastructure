#!/usr/bin/env bash

function techo {
    STAMP=`date '+%b %d %H:%M:%S'`
    echo "${STAMP} BOOTSTRAP: ${@}"
}


techo "start"
techo "Is ansible available in pip?"
if sudo pip search ansible; then
    sudo pip install ansible
    techo "Success in installing ansible with pip"
else
  techo "install rpm-build make python2-devel"
  yum -y install rpm-build make python2-devel
  techo "git clone ansible repo"
  git clone git://github.com/ansible/ansible.git --recursive
  techo "build ansible rpm"
  cd ./ansible
  make rpm
  sudo rpm -Uvh ./rpm-build/ansible-*.noarch.rpm
  techo "yum check-update"
  yum check-update
  techo "yum upgrade"
  yum -y upgrade
  techo "yum install puppet"
  yum -y install ansible
  techo "end"
fi
